# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
print(BASE_DIR)
#BASE_DIR = os.path.join(BASE_DIR, '..')
DATABASES = {
    'default': {'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
               }
}

INSTALLED_APPS = [
    "django.contrib.contenttypes",
    "django.contrib.auth",
    "tests",
    "fountain_groups",
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
    },
]

AUTH_USER_MODEL='auth.User'

SECRET_KEY = 'fake-key'
