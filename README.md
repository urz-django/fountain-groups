django-fountain-groups
======================

[![build status](https://gitlab.hrz.tu-chemnitz.de/urz-django/fountain-groups/badges/master/build.svg)](https://gitlab.hrz.tu-chemnitz.de/urz-django/fountain-groups/commits/master)

This library synchronizes Django group members from LDAP. Managed Groups are 
defined by creating a `FountainGroup` instance which references a Django Group. 
Group name in Django must equal Group `cn` Attribute in LDAP.
`FountainGroup`s can be used to manage `is_staff` and `is_superuser` flags in `User` model. See below.


Installation
------------

1. Add the following line to requirements.txt: `-e git+ssh://git@gitlab.hrz.tu-chemnitz.de/urz-django/fountain-groups.git@master#egg=django-fountain-groups`
2. Run `pip install -r requirements.txt`
3. Add `fountain_groups` to `INSTALLED_APPS`

Configuration
-------------

The following settings may be configured:

- `LDAP_GROUP_SYNC_URI`: This needs to be a LDAP **URI**, such as `ldaps://ldap.example.org/ou=Groups,dc=excample,dc=com`, defaults to `ldaps://ldap.tu-chemnitz.de/ou=Groups,dc=tu-chemnitz,dc=de`
- `LDAP_SYNC_BASE_USER`: Bind user DN. Uses anonymous bind if unset
- `LDAP_SYNC_BASE_PASS`: Bind user Password.
- `LDAP_CA_CERT`: TLS CA Chain to verify TLS connection, defaults to `None` which means to use the system CA store

Usage
-----

1. Apply migrations first: `./manage.py migrate`
2. Create `FountainGroup` in Django Admin
3. run `./manage.py fountain_sync_groups`


Manage `is_staff` and `is_superuser`
------------------------------------

You can manage `is_staff` and `is_superuser` flags of the Django `User` model 
using `FountainGroup` instances. **Attention**: As soon as **one** group has 
the `set_staff` set, `is_staff` of **all** Users are managed. The same applies 
to `set_superuser` and `is_superuser`. So be careful!


Using TUC IdM Groups
--------------------

1. Create Group foobar in IdM
    1. Manage members
    2. manage target systems, add public LDAP as target system
2. Create `FountainGroup` grp_foobar in Django Admin
3. run `./manage.py fountain_sync_groups`
