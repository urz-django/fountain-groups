# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.management.base import BaseCommand
from django.db import transaction
from fountain_groups.models import FountainGroup


class Command(BaseCommand):
    help = 'Sync FountainGroup instances with LDAP'

    @transaction.atomic()
    def handle(self, *args, **options):
        for fg in FountainGroup.objects.all():
            fg.sync_members()
        FountainGroup.sync_staff()
        FountainGroup.sync_superuser()
