# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from fountain_groups.models import FountainGroup


class FountainGroupAdmin(admin.ModelAdmin):
    list_display = ('get_group_name', 'set_staff', 'set_superuser', 'create_missing_users')
    search_fields = ('group__name',)

    def get_group_name(self, obj):
        return obj.group.name
    get_group_name.short_description = 'Name'
    get_group_name.admin_order_field = 'group__name'

admin.site.register(FountainGroup, FountainGroupAdmin)
