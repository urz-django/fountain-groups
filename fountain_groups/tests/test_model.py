# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase, override_settings
from django.contrib.auth.models import Group, User
from fountain_groups.models import FountainGroup
from ldap3 import Server, Connection, MOCK_SYNC

import mock

import os


class TestModel(TestCase):
    fixtures = ['fountain_groups/tests/foo']

    def _fake_ldap_connection(self, filename=None):
        if not filename:
            filename = "g1.json"
        basedir = "fountain_groups/fixtures/fountain_groups/ldap/"
        basedir = os.path.join(os.path.dirname(__file__), "..", "fixtures", "fountain_groups", "ldap")
        server = Server.from_definition(
            'my_fake_server',
            '%s/ldap.tu-chemnitz.de_info.json' % basedir,
            '%s/ldap.tu-chemnitz.de_schema.json' % basedir)
        conn = Connection(server, client_strategy=MOCK_SYNC)
        conn.strategy.entries_from_json('{}/{}'.format(basedir, filename))
        conn.bind()
        return conn

    def test_usernames(self):
        g1 = Group.objects.get(name="g1")
        fg1 = FountainGroup(group=g1, set_staff=False, set_superuser=False)
        fg1._ldap_connection = self._fake_ldap_connection()
        members = fg1.get_ldap_member_usernames()
        self.assertEqual(list(members), ["user1"])

    @override_settings(LDAP_GROUP_SYNC_URI='ldaps://ldap.tu-chemnitz.de/ou=Groups,dc=example,dc=com')
    def test_usernames_settings(self):
        g1 = Group.objects.get(name="g1")
        fg1 = FountainGroup(group=g1, set_staff=False, set_superuser=False)
        fg1._ldap_connection = self._fake_ldap_connection()
        members = fg1.get_ldap_member_usernames()
        self.assertEqual(list(members), [])

    def test_users(self):
        g1 = Group.objects.get(name="g1")
        fg1 = FountainGroup(group=g1, set_staff=False, set_superuser=False)
        fg1._ldap_connection = self._fake_ldap_connection()
        members = fg1.get_ldap_members()
        self.assertEqual(list(members), list(User.objects.filter(username="user1")))

    def test_sync_removes_users(self):
        u1 = User.objects.get(username="user1")
        g1 = Group.objects.get(name="g1")
        fg1 = FountainGroup(group=g1, set_staff=False, set_superuser=False)
        fg1._ldap_connection = self._fake_ldap_connection()
        self.assertEqual(list(g1.user_set.all()), [u1])
        u2 = User.objects.get(username="user2")
        g1.user_set.add(u2)
        g1.save()
        fg1.sync_members()
        self.assertEqual(list(g1.user_set.all()), [u1])

    def test_sync_adds_users(self):
        u1 = User.objects.get(username="user1")
        g1 = Group.objects.get(name="g1")
        fg1 = FountainGroup(group=g1, set_staff=False, set_superuser=False)
        fg1._ldap_connection = self._fake_ldap_connection()
        self.assertEqual(list(g1.user_set.all()), [u1])
        g1.user_set.remove(u1)
        g1.save()
        fg1.sync_members()
        self.assertEqual(list(g1.user_set.all()), [u1])

    def test_sync_staff_managed(self):
        g1 = Group.objects.get(name="g1")
        g2 = Group.objects.get(name="g2")
        fg1 = FountainGroup(group=g1, set_staff=False, set_superuser=False)
        fg1.save()
        fg2 = FountainGroup(group=g2, set_staff=False, set_superuser=False)
        fg2.save()

        self.assertEqual(FountainGroup.is_staff_managed(), False)
        fg1.set_staff = True
        fg1.save()
        self.assertEqual(FountainGroup.is_staff_managed(), True)

    def test_sync_superuser_managed(self):
        g1 = Group.objects.get(name="g1")
        g2 = Group.objects.get(name="g2")
        fg1 = FountainGroup(group=g1, set_staff=False, set_superuser=False)
        fg1.save()
        fg2 = FountainGroup(group=g2, set_staff=False, set_superuser=False)
        fg2.save()

        self.assertEqual(FountainGroup.is_superuser_managed(), False)
        fg1.set_superuser = True
        fg1.save()
        self.assertEqual(FountainGroup.is_superuser_managed(), True)

    def test_sync_staff(self):
        g1 = Group.objects.get(name="g1")
        fg1 = FountainGroup(group=g1, set_staff=False, set_superuser=False)
        fg1.save()
        # nothing managed at start, no staff user
        u1 = User.objects.get(username="user1")
        u2 = User.objects.get(username="user2")
        self.assertEqual(u1.is_staff, False)
        self.assertEqual(u2.is_staff, False)
        self.assertEqual(FountainGroup.is_staff_managed(), False)

        # the same after sync
        FountainGroup.sync_staff()
        u1 = User.objects.get(username="user1")
        u2 = User.objects.get(username="user2")
        self.assertEqual(u1.is_staff, False)
        self.assertEqual(u2.is_staff, False)

        # now manage
        fg1.set_staff = True
        fg1.save()
        FountainGroup.sync_staff()
        u1 = User.objects.get(username="user1")
        u2 = User.objects.get(username="user2")
        self.assertEqual(u1.is_staff, True)
        self.assertEqual(u2.is_staff, False)

    def test_sync_superuser(self):
        g1 = Group.objects.get(name="g1")
        fg1 = FountainGroup(group=g1, set_staff=False, set_superuser=False)
        fg1.save()
        # nothing managed at start, no staff user
        u1 = User.objects.get(username="user1")
        u2 = User.objects.get(username="user2")
        self.assertEqual(u1.is_superuser, False)
        self.assertEqual(u2.is_superuser, False)
        self.assertEqual(FountainGroup.is_superuser_managed(), False)

        # the same after sync
        FountainGroup.sync_staff()
        u1 = User.objects.get(username="user1")
        u2 = User.objects.get(username="user2")
        self.assertEqual(u1.is_superuser, False)
        self.assertEqual(u2.is_superuser, False)

        # now manage
        fg1.set_superuser = True
        fg1.save()
        FountainGroup.sync_superuser()
        u1 = User.objects.get(username="user1")
        u2 = User.objects.get(username="user2")
        self.assertEqual(u1.is_superuser, True)
        self.assertEqual(u2.is_superuser, False)

        #now remove
        fg1.group.user_set.remove(u1)
        FountainGroup.sync_superuser()
        u1 = User.objects.get(username="user1")
        u2 = User.objects.get(username="user2")
        self.assertEqual(u1.is_superuser, False)
        self.assertEqual(u2.is_superuser, False)

    def test_create_missing_users(self):
        g1 = Group.objects.get(name="g1")
        fg1 = FountainGroup(group=g1, set_staff=False, set_superuser=False, create_missing_users=True)
        fg1.save()
        m = mock.Mock()
        m.return_value = ['user1', 'foo']
        fg1.get_ldap_member_usernames = m
        fg1.sync_members()
        self.assertEqual(User.objects.filter(username="foo").count(), 1)

