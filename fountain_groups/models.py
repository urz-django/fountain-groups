# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, transaction
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.utils.translation import gettext_lazy as _

import ssl
from ldap3 import Server, Connection
try:
    # ldap3 Version 1.x
    from ldap3 import STRATEGY_SYNC_RESTARTABLE as RESTARTABLE
except ImportError:
    # ldap3 Version 2.x
    from ldap3 import RESTARTABLE
from ldap3.core.tls import Tls
from ldap3.utils.uri import parse_uri
try:
    from ldap3.utils.dn import escape_attribute_value
except ImportError:
    # ldap3 version >= 2.3
    from ldap3.utils.dn import _escape_attribute_value as escape_attribute_value

DEFAULT_LDAP_GROUP_SYNC_URI = 'ldaps://ldap.tu-chemnitz.de/ou=Groups,dc=tu-chemnitz,dc=de'
DEFAULT_CA_CERT = None # -> use system certificate store


def get_ldap_conn():
    LDAP_GROUP_SYNC_URI = getattr(settings, 'LDAP_GROUP_SYNC_URI', DEFAULT_LDAP_GROUP_SYNC_URI)
    LDAP_PARAMS = parse_uri(LDAP_GROUP_SYNC_URI)
    LDAP_SYNC_BASE_USER = getattr(settings, 'LDAP_SYNC_BASE_USER', None)
    LDAP_SYNC_BASE_PASS = getattr(settings, 'LDAP_SYNC_BASE_PASS', None)
    LDAP_CA_CERT = getattr(settings, 'LDAP_CA_CERT', DEFAULT_CA_CERT)

    if LDAP_PARAMS['ssl']:
        tls = Tls(ca_certs_file=LDAP_CA_CERT, validate=ssl.CERT_REQUIRED)
        s = Server(LDAP_PARAMS['host'], use_ssl=True, tls=tls)
    else:
        s = Server(LDAP_PARAMS['host'], use_ssl=False)
    conn = Connection(s,
        auto_bind=True,
        client_strategy=RESTARTABLE,
        user=LDAP_SYNC_BASE_USER,
        password=LDAP_SYNC_BASE_PASS
    )
    return conn


class FountainGroup(models.Model):
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    set_staff = models.BooleanField(
        help_text=_("automatically set is_staff field for all group members")
    )
    set_superuser = models.BooleanField(
        help_text=_("automatically set is_superuser field for all group members")
    )
    create_missing_users = models.BooleanField(default=False)

    def __init__(self, *args, **kwargs):
        self._ldap_connection = None
        super(FountainGroup, self).__init__(*args, **kwargs)

    def get_ldap_members(self):
        """
        returns a queryset of existing User instances that are members of the group
        """
        User = get_user_model()
        usernames = self.get_ldap_member_usernames()
        return User.objects.filter(username__in=usernames)

    def get_ldap_member_usernames(self):
        """
        return group members from LDAP
        """
        LDAP_GROUP_SYNC_URI = getattr(settings, 'LDAP_GROUP_SYNC_URI', DEFAULT_LDAP_GROUP_SYNC_URI)
        LDAP_PARAMS = parse_uri(LDAP_GROUP_SYNC_URI)
        if self._ldap_connection:
            conn = self._ldap_connection
        else:
            conn = get_ldap_conn()
        if conn.search(
            LDAP_PARAMS['base'],
            '(&(objectClass=posixGroup)(cn={}))'.format(escape_attribute_value(self.group.name)),
            attributes=('memberUid',),
        ):
            usernames = conn.response[0]['attributes']['memberUid']
            return usernames
        return []

    @transaction.atomic()
    def sync_members(self):
        if self.create_missing_users:
            User = get_user_model()
            usernames = set(self.get_ldap_member_usernames())
            existing_usernames = set(User.objects.filter(username__in=usernames).values_list("username", flat=True))
            missing_users = usernames - existing_usernames
            for username in missing_users:
                u = User(username=username)
                u.set_unusable_password()
                u.full_clean()
                u.save()

        want_users = set(self.get_ldap_members())
        current_users = set(self.group.user_set.all())
        to_remove = current_users - want_users
        to_add = want_users - current_users

        for u in to_remove:
            self.group.user_set.remove(u)
        for u in to_add:
            self.group.user_set.add(u)

    @classmethod
    def is_staff_managed(cls):
        return cls.objects.filter(set_staff=True).exists()

    @classmethod
    def is_superuser_managed(cls):
        return cls.objects.filter(set_superuser=True).exists()

    @classmethod
    @transaction.atomic()
    def sync_staff(cls):
        if not cls.is_staff_managed():
            return

        User = get_user_model()
        want_staff_usernames = set(cls.objects.filter(set_staff=True).values_list("group__user__username", flat=True))
        has_staff_usernames = set(User.objects.filter(is_staff=True).values_list("username", flat=True))

        remove_staff = has_staff_usernames - want_staff_usernames
        add_staff = want_staff_usernames - has_staff_usernames

        User.objects.filter(username__in=remove_staff).update(is_staff=False)
        User.objects.filter(username__in=add_staff).update(is_staff=True)

    @classmethod
    @transaction.atomic()
    def sync_superuser(cls):
        if not cls.is_superuser_managed():
            return

        User = get_user_model()
        want_superuser_usernames = set(cls.objects.filter(set_superuser=True).values_list("group__user__username", flat=True))
        has_superuser_usernames = set(User.objects.filter(is_superuser=True).values_list("username", flat=True))

        remove_superuser = has_superuser_usernames - want_superuser_usernames
        add_superuser = want_superuser_usernames - has_superuser_usernames

        User.objects.filter(username__in=remove_superuser).update(is_superuser=False)
        User.objects.filter(username__in=add_superuser).update(is_superuser=True)
