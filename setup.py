# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from setuptools import setup, find_packages

setup(name='django-fountain-groups',
      version='0.1',
      description='Syncronizes group membership of django groups from LDAP',
      author='Daniel Schreiber',
      author_email='daniel.schreiber@hrz.tu-chemnitz.de',
      url='https://gitlab.hrz.tu-chemnitz.de/urz-django/fountain-groups/',
      pacckages=find_packages(exclude=['tests']),
      install_requires=[
          'django>=1.8',
          'ldap3>=1.4.0',
      ],
)
